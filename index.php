<?php
	require 'php/functions.php';
	
	$page = 'index';
	$promo_book = db_query($db_books, 'SELECT * FROM books WHERE promo_book = 1')[0];
	$books = db_query($db_books, 'SELECT * FROM books WHERE promo_book = 0');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>Books</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
<!-- <pre><?php print_r($books); ?></pre> -->

	<?php require 'header.php'; ?>

	<div class="main-wrap">
		<main>
			<div class="jumbotron">
				<div class="container">
					<div class="book-cover">
						<h1><?= $promo_book["title"]; ?></h1>
						<img src="<?= $promo_book['img']; ?>" class="pull-left" width="200" height="250">
						<?= $promo_book["short_description"]; ?>
						<p><a href="book.php?id=<?= $promo_book['id']; ?>" class="btn btn-primary btn-lg btn-book">Read more</a></p>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<?php for($i = 0; $i < count($books); $i++) : ?>
						<?php $book = $books[$i] ?>
						<div class="book-wrap col-md-4 col-sm-6">
							<h3><?= $book["title"]; ?></h3>
							<div class="content clearfix">	
								<img src="<?= $book['img']; ?>" class="pull-left">
								<?= $book["short_description"]; ?>
							</div>
							<p class="text-right"><a href="book.php?id=<?= $book['id']; ?>" class="btn btn-primary btn-book">Read more</a></p>
						</div>
					<?php endfor ?>
			   </div>
			</div>
		</main>
	</div>

	<?php require 'footer.html'; ?>
	<?php require 'modal-register.html'; ?>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.js"></script>
</body>
</html>