<?php
	require 'php/functions.php';

	if (!isset($_GET["id"])) header("location: /books");

	$book = db_query($db_books, 'SELECT * FROM books WHERE id = ?', [$_GET["id"]]);

	if (!isset($book[0])) header("location: /books");

	$book = $book[0];
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $book["title"]; ?></title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/app.css">
	<link rel="stylesheet" href="css/book.css">
</head>
<body>
	<header>
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<p class="welcome-copy pull-right visible-xs d-n lh-50">Welcome, <span></span></p>
					<a class="navbar-brand" href="index.php">Books</a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">About</a></li>
						<li><a href="mailto:info@books.com">Email Us</a></li>
					</ul>
					<form id="sign-in-form" class="form-inline d-n">
						<div class="form-group input-group">
							<input type="text" name="email" id="email" class="form-control" placeholder="Email">
								<span class="input-group-btn">
						      	<button class="btn btn-default" type="submit">Sign In</button>
						    </span>
						</div>
					</form>
					<form id="sign-out-form" class="d-n">
						<div class="form-group">
							<p class="welcome-copy hidden-xs">Welcome, <span></span></p>
					    	<button class="btn btn-default" type="submit">Sign Out</button>
						</div>
					</form>	
				</div>
			</div>
		</nav>
	</header>
	<div class="main-wrap">
		<main class="container">
			<h2><?= $book["title"]; ?></h2>
			<div class="image">
				<img src="<?= $book['img']; ?>" alt="<?= $book['title']; ?>">
		    </div>
			<div class="book-info">
		    	<table>
		    		<tbody>
			    		<tr>
			    			<td>Author</td>
			    			<td><?= $book["author"]; ?></td>
			    		</tr>		
						<tr>
			    			<td>Country</td>
			    			<td><?= $book["country"]; ?></td>
			    		</tr>		
						<tr>
			    			<td>Language</td>
			    			<td><?= $book["language"]; ?></td>
			    		</tr>		
						<tr>
			    			<td>Genre</td>
			    			<td><?= $book["genre"]; ?></td>
			    		</tr>
			    		<tr>		
							<td>Publisher</td>
			    			<td><?= $book["publisher"]; ?></td>
			    		</tr>
			    		<tr>
			    			<td>Publication date</td>
			    			<td><?= date("Y", strtotime($book["publication_date"])); ?></td>
			    		</tr>
			    		<tr>
							<td>Pages</td>
			    			<td><?= $book["pages"]; ?></td>
			    		</tr>
		    		</tbody>
		    	</table>
		    	<div>
		    		<?= $book["full_description"]; ?>
		    	</div>
				<p class="text-right"><a href="<?= $book['url']; ?>" target="_blank" class="btn btn-primary">Read book</a></p>
			</div>
		</main>
	</div>
	<footer>
		<div class="container">
			<ul class="pull-left">
				<li><a href="facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="instagram.com"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
			<p class="pull-right">Books Inc.&copy;</p>
		</div>
	</footer>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.js"></script>
</body>
</html>	



