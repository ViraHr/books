window.getCookie = function(name) {
	var cookies = document.cookie.split(";");

	for (let i = 0; i < cookies.length; i++) {
		var cookie = cookies[i].split("=");

		if (cookie[0].trim() == name) return unescape(cookie[1]);
	}
};

window.createCookie = function(name, value) {
	var exdays = 3650,
		exdate = new Date();

	exdate.setDate(exdate.getDate() + exdays);

	document.cookie = name + "=" + value + (";expires=" + exdate.toUTCString());
};

window.deleteCookie = function(name) {
	document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

if(window.getCookie('email')) {
	$("#sign-out-form").add(".welcome-copy").removeClass("d-n");
	$(".welcome-copy").children("span").html(window.getCookie("name"))
} else {
	$("#sign-in-form").removeClass("d-n");

	var path = location.pathname;

	if (path !== '/books/' && path !== '/books/index.php' && path !== '/books/about.php') {
		alert("Please sign in");

		location = "/books/";
	} else {
		$(".btn-book").on("click", function(e) {
			e.preventDefault();

			alert("Please sign in");
		});
	}
}

$(function() {
	function setDynamicHeight(element) {
		element.height("auto");

		var heights = element.map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);

		element.height(maxHeight);
	}

	var $content = $(".content"),
		$title   = $(".book-wrap > h3");

	setDynamicHeight($content);
	setDynamicHeight($title);

	$(window).on("resize", function() {
		if (window.innerWidth > 767) {
			setDynamicHeight($content);
			setDynamicHeight($title);
		} else {
			$content.add($title).height("auto");
		}
	});

	$("#sign-in-form").on("submit", function(e) {
		e.preventDefault();

		var email = $("#email").val();

		if (/\w+@\w+\.\w{2,}/g.test(email)) {
			$.ajax({
				url: "php/check-user.php?email=" + email,
				success: function(user) {
					// console.log(user);

					if (user == 0) {
						$('#myModal').modal('show');
					} else {
						user = JSON.parse(user);

						window.createCookie("name", user.name);
						window.createCookie("email", email);
						location.reload();
					}
				}
			});
		} else {
			alert("Invalid Email.");
		}
	});

	$("#sign-up-form").on("submit", function(e) {
		e.preventDefault();

		var $form = $("#sign-up-form"),
			name  = $form.find("#name").val(),
			email = $("#email").val();

		$("#sign-up-email").val(email);

		if (name.length > 0) {
			$.ajax({
				url: "php/sign-up.php",
				method: "POST",
				data: $form.serialize(),
				success: function(response) {
					console.log(response);

					window.createCookie("name", name);
					window.createCookie("email", email);
					location.reload();
				}
			});
		}
	});
	$("#sign-out-form").on("submit", function(e) {
		e.preventDefault();

		window.deleteCookie("name");
		window.deleteCookie("email");
		location = "/books/";
	});
});
