<header>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<p class="welcome-copy pull-right visible-xs d-n lh-50">Welcome, <span></span></p>
				<a class="navbar-brand" href="index.php">Books</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li <?php if ($page == "index") echo'class="active"'; ?>><a href="index.php">Home</a></li>
					<li <?php if ($page == "about") echo'class="active"'; ?>><a href="about.php">About</a></li>
					<li><a href="mailto:info@books.com">Email Us</a></li>
				</ul>
				<form id="sign-in-form" class="form-inline d-n">
					<div class="form-group input-group">
						<input type="text" name="email" id="email" class="form-control" placeholder="Email">
							<span class="input-group-btn">
					      	<button class="btn btn-default" type="submit">Sign In</button>
					    </span>
					</div>
				</form>
				<form id="sign-out-form" class="d-n">
					<div class="form-group">
						<p class="welcome-copy hidden-xs">Welcome, <span></span></p>
				    	<button class="btn btn-default" type="submit">Sign Out</button>
					</div>
				</form>	
			</div>
		</div>
	</nav>
</header>
