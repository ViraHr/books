<?php $page = 'about'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>Books</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<?php require 'header.php'; ?>

	<div class="main-wrap">
		<main class="container">
			<div class="row">
				<div>
					<h2 class="text-center">About Us</h2>
					<p class="text-center"><strong>Welcome to our Online BookStore!</strong></p>
					<p>
						We are always upgrading Bookstore. Please search or browse our inventory to find an interesting book for you.<br>
						If you have any questions please email us!<br>
						Enjoy your time with our Online Bookstore!<br>
					</p>
				</div>
				<div>
					<p>
						Here are some famous motivational quotes on reading benefits:<br>
						<em>“Books are the plane, and the train, and the road. They are the destination, and the journey. They are home.”</em> - Anna Quindlen<br>
						<em>“The best advice I ever got was that knowledge is power and to keep reading.”</em> - David Bailey<br>
						<em>“Not all leaders are readers, but all readers are leaders.”</em> - Harry Truman
					</p>
				</div>
				<div class="text-center mt-20">	
					<img src="https://cdn.aarp.net/content/dam/aarp/money/budgeting_savings/2016/04/1140-yeager-sell-your-used-books.imgcache.rev6feda141288df73e8fd100822bb375ea.jpg" width="500" height="300">
				</div>
			</div>
		</main>
	</div>

	<?php require 'footer.html'; ?>
	<?php require 'modal-register.html'; ?>

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.js"></script>
</body>
</html>