<?php

require 'functions.php';

$user = db_query($db_books, 'SELECT name FROM users WHERE email = ?', [$_GET["email"]]);

if (!isset($user[0])) {
	echo 0;
} else {
	echo json_encode($user[0]);
}

?>