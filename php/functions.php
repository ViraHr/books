<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	$db_books = new PDO('mysql:host=localhost;dbname=books', 'books_admin', 'qwerty');
	
	function db_query($conn, $query, $args = NULL) {
		$query = $conn->prepare($query);

		if (is_null($args)) {
			$query->execute();
		} else {
			$query->execute($args);
		}
		
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}
?>